---
title: Jayant Narlikar's pseudo-defence of Darwin
date: 2015-01-05
---

Jayant Narlikar, the noted astrophysicist and emeritus professor at the Inter-University Centre for Astronomy and Astrophysics, Pune, recently [wrote an op-ed](http://www.thehindu.com/opinion/lead/science-should-have-the-last-word/article22777732.ece) in _The Hindu_ titled ‘Science should have the last word’. There’s probably a tinge of sanctimoniousness there, echoing the belief many scientists I’ve met have that science will answer everything, often blithely oblivious to politics and culture. But I’m sure Narlikar is not one of them.

Nonetheless, the piece IMO was good and not great because what Narlikar has written has been written in the recent past by many others, with different words. It was good because the piece’s author was Narlikar. His position on the subject is now in the public domain where it needs to be if only so others can now bank on his authority to stand up for science themselves.

Speaking of authority: there is a gaffe in the piece that its fans – and _The Hindu_‘s op-ed desk – appear to have glazed over. If they didn’t, it’s possible that Narlikar asked for his piece to be published without edits, and which could have either been further proof of sanctimoniousness or, of course, distrust of journalists. He writes:

> Recently, there was a claim made in India that the Darwinian theory of evolution is incorrect and should not be taught in schools. In the field of science, the sole criterion for the survival of a theory is that it must explain all observed phenomena in its domain. For the present, Darwin’s theory is the best such theory but it is not perfect and leaves many questions unanswered. This is because the origin of life on earth is still unexplained by science. However, till there is a breakthrough on this, or some alternative idea gets scientific support, the Darwinian theory is the only one that should continue to be taught in schools.

@avinashtn, @thattai and @rsidd120 got the problems with this excerpt, particularly the part in bold, just right in a short Twitter exchange.

Gist: the origin of life is different from the evolution of life.

But even if they were the same, as Narlikar conveniently assumes in his piece, something else should have stopped him. That something else is also what is specifically interesting for me. Sample what Narlikar said next and then the final line from the excerpt above:

> For the present, Darwin’s theory is the best such theory but it is not perfect and leaves many questions unanswered. … However, till there is a breakthrough on this, or some alternative idea gets scientific support, the Darwinian theory is the only one that should continue to be taught in schools.

Darwin’s theory of evolution got many things right, continues to, so there is a sizeable chunk in the domain of evolutionary biology where it remains both applicable and necessary. However, it is confusing that Narlikar believes that, should some explanations for some phenomena thus far _not understood_ arise, Darwin’s theories as a whole could become obsolete. But why? It is futile to expect a scientific theory to be able to account for “all observed phenomena in its domain”. Such a thing is virtually impossible given the levels of specialisation scientists have been able to achieve in various fields. For example, an evolutionary biologist might know how migratory birds evolved but still not be able to explain how some birds are [thought to use](http://physicsworld.com/cws/article/news/2016/apr/07/birds-measure-magnetic-fields-using-long-lived-quantum-coherence) quantum entanglement with Earth’s magnetic field to navigate.

The example [Mukund Thattai provides](https://twitter.com/thattai/status/964885318724964353) is fitting. The Navier-Stokes equations are used to describe fluid dynamics. However, scientists have been studying fluids in a variety of contexts, from two-dimensional vortices in liquid helium to gas outflow around active galactic nuclei. It is only in some of these contexts that the Navier-Stokes equations are applicable; that they are not entirely useful in others doesn’t render the equations themselves useless.

Additionally, this is where Narlikar’s choice of words in his op-ed becomes more curious. He must be aware that his own branch of study, quantum cosmology, has thin but unmistakable roots in a principle conceived in the 1910s by Niels Bohr, with many implications for what he says about Darwin’s theories.

Within the boundaries of physics, the principle of correspondence states that at larger scales, the predictions of quantum mechanics must agree with those of classical mechanics. It is an elegant idea because it acknowledges the validity of classical, a.k.a. Newtonian, mechanics when applied at a scale where the effects of gravity begin to dominate the effects of subatomic forces. In its statement, the principle does not say that classical mechanics is useless because it can’t explain quantum phenomena. Instead, it says that (1) the two mechanics each have their respective domain of applicability and (2) the newer one must be resemble the older one when applied to the scale at which the older one is relevant.

Of course, while scientists have been able to satisfy the principle of correspondence in some areas of physics, an overarching understanding of gravity as a quantum phenomenon has remained elusive. If such a theory of ‘quantum gravity’ were to exist, its complicated equations would have to be able to resemble Newton’s equations and the laws of motion at larger scales.

But exploring the quantum nature of spacetime is extraordinarily difficult. It requires scientists to probe really small distances and really high energies. While lab equipment has been setup to meet this goal partway, it has been clear for some time that it might be easier to learn from powerful cosmic objects like blackholes.

And Narlikar has done just that, among other things, in his career as a theoretical astrophysicist.

I don’t imagine he would say that classical mechanics is useless because it can’t explain the quantum, or that quantum mechanics is useless because it can’t be used to make sense of the classical. More importantly, should a theory of quantum gravity come to be, should we discard the use of classical mechanics all-together? No.

In the same vein: should we continue to teach Darwin’s theories for lack of a better option or because it is scientific, useful and, through the fossil record, demonstrable? And if, in the future, an overarching theory of evolution comes along with the capacity to subsume Darwin’s, his ideas will still be valid in their respective jurisdictions.

As Thattai says, “Expertise in one part of science does not automatically confer authority in other areas.” Doesn’t this [sound familiar](http://philosophy.lander.edu/logic/authority.html)?

_Featured image credit: sipa/pixabay._